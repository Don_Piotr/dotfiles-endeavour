# Firefox
- Ctrl+L, Shift+Enter - apre questa pagina in nuova finestra

# WM
(Alcune cpombinazioni di tasti)
- Super+Enter - apre nuovo terminale con tmux
- Super+Shift+Enter - apre nuovo terminale chiedendo cosa aprire dentro
- Super+U - apre/chiude alacritty come scratchpad
- Super+D - avvia applicazione (solo .desktop)
- Super+R - avvia applicazione (tutti esecutibili)
- Super+Shift+E - mostra menu di spegnimento
- Super+Alt+Q - esce da WM
- Super+Alt+R - riavvia WM
- Super+Q - chiude applicazione
- Super+Shift+Q - ammazza applicazione
- Ctrl+Alt - cambia layout della tastiera

# WM (maneggiare le fineste)
- Super+G - scambia finestra con la più grande
- Super+T - imposta finestra come piastrella
- Super+F - imposta finestra come fluttuante
- Super+A - imposta finestra su tutto schermo
- Super+Spazio - sposta focus sulla finestra fluttuante
- Super+Freccia - sposta focus tra le finestre a piastrella
- Super+Shift+Freccia - scambia le finestre a piastrella
- Super+Cifra - mostra il desktop indicato
- Super+Shift+Cifra - sposta finestra su desktop indicato
- Super+Tab - metti focus sul monitor seguente
- Super+Shift+Tab - sposta la finestra su monitor seguente
- 
- Super+Shift+B - mostra / nasconde la barra
- Print - scatta schermo
- Super+Print - scatta scermo dopo aver scelto la area

# Notifiche
- Super+B - info sulla batteria
- Super+N - info sugli IP

# SXHKD
- Super+Esc - ricarica la configurazione

# Tmux
- Ctrl + Up - apre nuovo terminale
- Ctrl + Left/Right - cambia il terminale

# Vim
