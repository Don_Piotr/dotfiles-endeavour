module BspRofiMenu
  @@menu = {
    "....Lock" => "i3lock -c 000000 -e -i ~/Wallpapers/DeltaBlackTenComCath.png",
    "....Restart WM" => "bspc wm -r",
    "....Logout" => "bspc quit",
    "....Shutdown" => "systemctl poweroff",
    "....Reboot" => "systemctl reboot",
    "...Sleep" => "amixer set Master mute && systemctl suspend"
  }
end

