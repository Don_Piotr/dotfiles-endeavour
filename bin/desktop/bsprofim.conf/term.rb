module BspRofiMenu
  term = "/usr/bin/xfce4-terminal"
  @@menu = {
    "Solo terminale" => term,
    "tmux" => "#{term} -e tmux",
    "mc" => "#{term} -e mc",
    "lf" => "#{term} -e lf",
    "irb" => "#{term} -e irb",
    "nmtui" => "#{term} -e nmtui"
  }
end

