module BspRofiMenu
  @@menu = {
    ": 󰩨" => "bspc node -s biggest.local.window",
    ": " => "bspc node -t tiled",
    ": ~" => "bspc node -t pseudo_tiled",
    ": " => "bspc node -t floating",
    ": " => "bspc node -t fullscreen",
    ": " => "bspc node -g sticky",
    ":   " => "bspc desktop -l next"
  }
end

